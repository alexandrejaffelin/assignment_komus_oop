"""
Question 1

Write a program that computes the prime number from 1 to N.

A prime number is a number only divisible by 1 and itself


input_number = int(input("Enter a number to know if it's a prime number"))

if input_number > 1:
  for num in range(2, input_number):
    if (input_number % num) == 0:
      print(num, "is not a prime number")
      break
    else:
      print(num, "is a prime number")
else:
  print(input_number, "is not a prime number")


Question Two

compute the cube sum of first n natural numbers

def sum_cube(num):
  if num < 0:
    return num
  sum = 0
  for n in range(num+1):
    sum += pow(n, 3)
  return sum

lets_cube_it = int(input("Input a number please"))
sum_cube_it = sum_cube(lets_cube_it)
print(sum_cube_it)


Question Three

Given an array of integers nums and an integer target, return indices of the two numbers such that they add up to target.

You may assume that each input would have exactly one solution, and you may not use the same element twice. You can return the answer in any order.

class Question_3():
  def sum_to_target(self, sums, target):
    self.sums = []
    self.target = int
    for i in range(len(sums)):
      for j in range(i + 1, len(sums)):
        if sums[i] + sums[j] == target:
          return i, j

a = Question_3()
print(a.sum_to_target([3,3,4,4,5,5], 7))


Question 4
Create a class for string operation. The class Constructor accepts a string

Methods

Get length of string
Reverse the string
if length > 5: get the first three characters
if length > 6: get the last 4 characters
returns character in a position. This method accepts int. if the inputted int is less than the length of the string, return the character in position [int], else return error


Test

Write test for all your methods



class String:

  def __init__(self):
    self.str = input("Enter a word or a small sentence")

  def get_len_str(self):
    return len(self.str)

  def reverse_str(self):
    return self.str[::-1]

  def get_three_letters(self):
    if self.str > str(5):
      return self.str[0:3]

  def get_last_4(self):
    if self.str > str(6):
      return self.str[-4:]

  def char_position(self):
    integer = int(input("Enter a number"))
    if integer < len(self.str):
      return self.str[integer-1]


a = String()
print(a.get_len_str())
print(a.reverse_str())
print(a.get_three_letters())
print(a.get_last_4())
print(a.char_position())
"""